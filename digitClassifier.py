from random import uniform, seed, sample
from os import path
from math import exp, sqrt, fsum
from copy import deepcopy
from sys import float_info
from numpy import dot, array, add, append, zeros, copy, testing

MAX_GREYSCALE = 255
OUTPUT_SIZE = 10
INPUT_SIZE = 784
ZERO_VECTOR = zeros(OUTPUT_SIZE)
ITERATIONS = 50
NEG_INF = float_info.max * -1
seed(1)


    
class TestCase:
    def __init__(self, inputVec, value, outPutSize):
        self.inputVec = array(inputVec)
        self.value = value
        self.outPutSize = outPutSize

    def getOutputVector(self):
        ret = array([0.1 for _ in xrange(self.outPutSize)])
        ret[self.value] = .9
        return ret
    
    def cost(self, actual):
        dif = [e_i - a_i for e_i, a_i in zip(self.getOutputVector(), actual)]
        vecLen = sqrt(sum(i**2 for i in dif))
        return (vecLen**2)/2.0

class Perceptron:
    def __init__(self, numInputs):
        #first element is the bias
        self.weights = array([uniform(-.05, .05) for _ in xrange(numInputs + 1)])
        self.wDelta = self.resetDeltas()
        self.inputs = None
        self.value = None
        self.error = None
    
    def resetDeltas(self):
        return zeros(len(self.weights))
    
    def setInputs(self, inputs):
        self.inputs = append([1.0], inputs)

    def exe(self):
        val = dot(self.inputs, self.weights)
        val = self.activation(val)
        self.value = val
        return val

    def activation(self, x):
        return 1.0 / (1.0 + exp(-x))


#Returns two dimensional array
#first element is the desired classification and the rest is input data
#all inputs are scaled to be a decimal from 0 to 1 based on gray scale density
def getDataFromFile(fp):
    testCases = []

    with open(fp) as fh:
        for line in fh:
            instance = line.split(',')
            expected = int(instance.pop(0))
            inputs = [float(x) / float(MAX_GREYSCALE) for x in instance]
            testCases.append(TestCase(inputs, expected, OUTPUT_SIZE))
    
    return testCases

    

class NeuralNet():
    def __init__(self, dims):
        self.perceps = []
        
        for level in range(1,len(dims)):
            layer = []
            for i in xrange(dims[level]):
                #input to each layer will be the size of the previous
                layer.append(Perceptron(dims[level -1]))
            self.perceps.append(layer)
        
        self.numLayers = len(self.perceps)
    
    def exe(self, inputVec):
        data = inputVec
        
        #execute each layer at a time
        for layer in self.perceps: 
            for p in layer:
                p.setInputs(data)
                value = p.exe()
            
            #input into next layer will be result of last
            data = [p.value for p in layer]
        
        #figure out the perceptron with largest output
        max = data[0]
        index = 0
        for i in xrange(len(data)):
            if data[i] > max:
                max = data[i]
                index = i
       
        return (max, index)
        
    def calcError(self, expected, update = True, lr = .1, mom = 0.9):
        #starting with last layer
        for layNum in reversed(xrange(self.numLayers)):
            for pIndex in xrange(len(self.perceps[layNum])):
                percep = self.perceps[layNum][pIndex]
                errorFactor = 0
                
                #output is the error in output and 
                #inner is based on error of outter
                if layNum == self.numLayers -1:
                    errorFactor =  expected[pIndex] - percep.value
                else:
                    for outterP in self.perceps[layNum + 1]:
                        errorFactor += outterP.weights[pIndex] * outterP.error             

                if update:
                    #calculate error
                    percep.error = percep.value * (1 - percep.value) * errorFactor
                    wDeltaVec = (lr * percep.error) * percep.inputs
                    partialCalc = add(wDeltaVec, mom * percep.wDelta)
                    percep.weights = add(percep.weights, partialCalc)
                    percep.wDeltaVec = wDeltaVec
                    
                    #percep.error = percep.value * (1 - percep.value) * errorFactor
                    #for i in xrange(len(percep.weights)):
                    #    wDelta = lr * percep.error * percep.inputs[i] 
                    #    percep.weights[i] += wDelta + (mom * percep.wDelta[i])
                    #    percep.wDelta[i] = wDelta 

#nn = NeuralNet([2,3,5,2], .1)
#nn = NeuralNet([2,3,2])

#tests = [([0.0,0.0], 0, 2),([0.0,1.0], 1, 2),([1.0,0.0], 1, 2),([1.0,1.0], 0, 2)]
#testCases = []
#for (input, expected, outputSize) in tests:
#    testCases.append(TestCase(input, expected, outputSize))

#for x in xrange(50000):
#    for update in [True, False]:
#        for testCase in testCases:
#            print(testCase.inputVec)
#            value, index = nn.exe(testCase.inputVec)    
#            print("\t" + str(index))
#            for p in nn.perceps[-1]:
#                print("\t\t" + str(p.value))
#            nn.calcError(testCase.getOutputVector(), update)
    

if __name__ == "__main__":
    #pull training and test data from files
    trainDataPath = path.join(path.dirname(__file__), "mnist_train.csv")
    testDataPath = path.join(path.dirname(__file__), "mnist_test.csv")

    test_testCases = getDataFromFile(testDataPath)
    train_testCases = getDataFromFile(trainDataPath)

    #run through data 3 times to see progrerss
    testSets = [train_testCases, test_testCases, train_testCases]
    testTypes = ["Training Data", "Test Data", "Updating"]
    updateFlags = [False, False, True]

    for nodes in [20, 50, 100]:
        print("Nodes: " + str(nodes))
        #create output perceptrons
        nn = NeuralNet([INPUT_SIZE,nodes,OUTPUT_SIZE])
            
        trainRet = []
        testRet = []
        for epoch in xrange(ITERATIONS):
            for testSet, update, testType in zip(testSets, updateFlags, testTypes):
                numCorrect = 0
                
                #10 by 10 table with all values set to 0
                confMatrix = None
                if testType == "Test Data":
                    confMatrix = [[0 for x in xrange(OUTPUT_SIZE)] for y in xrange(OUTPUT_SIZE)]           
                
                for testCase in testSet:
                    
                    value, index = nn.exe(testCase.inputVec) 
                    if update:
                        nn.calcError(testCase.getOutputVector())
                    #only update weights if output is incorrect
                    if testCase.value == index:
                        numCorrect += 1
                    
                    if testType == "Test Data":
                        confMatrix[testCase.value][index] += 1
                
                #print results for writeup
                if not update:
                    print("Epoch: " + str(epoch))
                    print("Type: " + testType)
                    result = numCorrect/float(len(testSet))
                    print(str(result) + " correct")
                    
                    if testType == "Test Data":
                        for x in xrange(len(confMatrix)):
                            print(confMatrix[x])
                        testRet.append(result)
                    else:
                        trainRet.append(result)
                        
        for x in trainRet + [""] + testRet:
            print(x)
