from random import uniform, seed, sample
from os import path
from math import exp, sqrt, fsum
from copy import deepcopy
from sys import float_info
import numpy as np

MAX_GREYSCALE = 255
OUTPUT_SIZE = 10
INPUT_SIZE = 784
ZERO_VECTOR = np.zeros(OUTPUT_SIZE)
ITERATIONS = 50
NEG_INF = float_info.max * -1
seed(1)


    
class TestCase:
    def __init__(self, inputVec, value, outPutSize):
        self.inputVec = np.array(inputVec)
        self.value = value
        self.outPutSize = outPutSize
        self.outVec = self.getOutputVector()

    def getOutputVector(self):
        ret = np.array([0.1 for _ in xrange(self.outPutSize)])
        ret[self.value] = .9
        return ret
    
    def cost(self, actual):
        dif = [e_i - a_i for e_i, a_i in zip(self.getOutputVector(), actual)]
        vecLen = sqrt(sum(i**2 for i in dif))
        return (vecLen**2)/2.0


#Returns two dimensional array
#first element is the desired classification and the rest is input data
#all inputs are scaled to be a decimal from 0 to 1 based on gray scale density
def getDataFromFile(fp):
    testCases = []

    with open(fp) as fh:
        for line in fh:
            instance = line.split(',')
            expected = int(instance.pop(0))
            inputs = [float(x) / float(MAX_GREYSCALE) for x in instance]
            testCases.append(TestCase(inputs, expected, OUTPUT_SIZE))
    
    return testCases

    

class NeuralNet():
    #dim is array of integers representing the numer input, hiden *, output nodes
    def __init__(self, dims):
        #initialize weights to random numbers. Bias accounted for
        #self.weights[layer][percep][InputFromLastNode]
        self.weights = []
        self.oldDeltas = []

        #ignore input layer and only count perceptron layers
        self.numLayers = len(dims) - 1
        
        #create weights for each perceptron layer
        for index in range(1,len(dims)):
            self.weights.append([])
            self.oldDeltas.append([])
            for perceps in xrange(dims[index]):
                #number of weights is numer of inputs from previous + bias
                self.weights[index - 1].append([uniform(-.05, .05) for _ in xrange(dims[index - 1] + 1)])
                self.oldDeltas[index - 1].append([0 for _ in xrange(dims[index - 1] + 1)])
        
        #convert lists into vectroized array
        self.weights = np.array(self.weights)
        self.oldDeltas = np.array(self.oldDeltas)
        
        #allows for effective sigma calulation of a layer output
        self.act = np.vectorize(self.activations)
    
    #run neural network on given input.  Update weights if requested
    #return the perceptron index of output layer with highest activation
    def exe(self, inputVec, backProp = False, expected = None, lr = .1, mom = .9):
        vec = np.array(inputVec)
        inputs = [np.append(1, vec)]
        results = []

        
        for layerWeights in self.weights:
            #calculate dot product for each perceptron
            output = np.inner(inputs[-1], layerWeights)
            
            #run sigmoid function
            output = self.act(output)
            
            #store results for next layers execution
            results.append(output)
            inputs.append(np.append(1, output))# add bias
        
        #remove inputs to the layer tht does not exist
        del inputs[-1]
        
        if not backProp:
            return np.argmax(results[-1])
       
        #calculate the error for each perceptron starint with last layer
        errors = [[] for x in range(self.numLayers)]
        for lNum in reversed(xrange(self.numLayers)):
            errorCond = None
            
            #output layer
            if lNum == self.numLayers - 1:
                #expected - actual
                errorCond = np.subtract(expected, results[lNum])
            #hidden layer
            else:
                #find weights from next layer and remove the bias weight
                actWeights = np.delete(self.weights[lNum + 1], 1,-1)
                #error = errorOutter dot weights to current node
                errorCond = np.inner(errors[lNum + 1], actWeights.T)
                
            #errorVec * outputVec * (1 - outputVec)
            errors[lNum] = np.multiply(np.multiply(errorCond, results[lNum]), 1 - results[lNum])
            
        #weights -> oldWeight + learningRate * (error * input) + momentup * olddelta
        for x in xrange(self.numLayers):
            deltas = (lr * (np.outer(errors[x], inputs[x])))
            self.weights[x] = self.weights[x] + deltas + np.multiply(mom, self.oldDeltas[x])
            self.oldDeltas[x] = deltas
        
        return np.argmax(results[-1])
        
    #sigmod fuction
    def activations(self, x):
        return 1.0 / (1.0 + exp(-x))

if __name__ == "__main__":
    #pull training and test data from files
    trainDataPath = path.join(path.dirname(__file__), "mnist_train.csv")
    testDataPath = path.join(path.dirname(__file__), "mnist_test.csv")

    test_testCases = getDataFromFile(testDataPath)
    train_testCases = getDataFromFile(trainDataPath)
    
    #run through data 3 times to see progrerss
    testSets = [train_testCases, test_testCases, train_testCases]
    testTypes = ["Training Data", "Test Data", "Updating"]
    updateFlags = [False, False, True]

    for mom in [0.0, .025, .5]:
        print("Momentum: " + str(mom))
        #create output perceptrons
        nn = NeuralNet([INPUT_SIZE,100,OUTPUT_SIZE])
            
        trainRet = []
        testRet = []
        for epoch in xrange(ITERATIONS):
            print(epoch)
            for testSet, update, testType in zip(testSets, updateFlags, testTypes):
                numCorrect = 0
                
                #10 by 10 table with all values set to 0
                confMatrix = None
                if testType == "Test Data":
                    confMatrix = [[0 for x in xrange(OUTPUT_SIZE)] for y in xrange(OUTPUT_SIZE)]           
                
                #run each sample
                for testCase in testSet:
                    index = nn.exe(testCase.inputVec, update, testCase.outVec, .1, mom) 
                    if testCase.value == index:
                        numCorrect += 1
                    
                    if testType == "Test Data":
                        confMatrix[testCase.value][index] += 1
                
                #print results for writeup
                if not update:
                    print("Epoch: " + str(epoch))
                    print("Type: " + testType)
                    result = numCorrect/float(len(testSet))
                    print(str(result) + " correct")
                    
                    if testType == "Test Data":
                        for x in xrange(len(confMatrix)):
                            print(confMatrix[x])
                        testRet.append(result)
                    else:
                        trainRet.append(result)
        
        for x in trainRet + [""] + testRet:
            print(x)
